<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkedBox</name>
   <tag></tag>
   <elementGuidId>fc547f11-3c51-4c47-8758-e0e0737bc547</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.css-e8bjan > svg.unf-icon > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>d219c38e-a943-4c87-92e8-bf6eb780db97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M10.5 15.75a.74.74 0 0 1-.53-.22l-3-3A.75.75 0 0 1 8 11.47l2.47 2.47L16 8.47a.75.75 0 0 1 1 1.06l-6 6a.74.74 0 0 1-.5.22Z</value>
      <webElementGuid>0b02cb21-ef11-41db-86cc-f1d5a23e0e16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;css-14u6y52&quot;]/div[@class=&quot;css-8atqhb e1iwxiko0&quot;]/div[@class=&quot;css-17uduf3&quot;]/div[@class=&quot;css-nsmt50&quot;]/div[@class=&quot;css-lbroap&quot;]/div[@class=&quot;css-1m9l7hb&quot;]/section[@class=&quot;css-q1vfvh-unf-card eeeacht0&quot;]/div[@class=&quot;css-16j44w0&quot;]/div[@class=&quot;date&quot;]/div[@class=&quot;css-k008qs&quot;]/div[@class=&quot;inner-title css-4g6ai3&quot;]/div[@class=&quot;css-1bh52ki e3y1k2n0&quot;]/label[@class=&quot;checkbox css-8wwjx7-unf-checkbox e3y1k2n2&quot;]/span[@class=&quot;css-1i85qm8-unf-checkbox__area e3y1k2n1&quot;]/div[@class=&quot;css-e8bjan&quot;]/svg[@class=&quot;unf-icon&quot;]/path[1]</value>
      <webElementGuid>0f731e1d-a703-423b-be57-c3ddd9b9cf2a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
