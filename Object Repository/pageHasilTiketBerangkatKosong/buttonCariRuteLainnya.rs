<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonCariRuteLainnya</name>
   <tag></tag>
   <elementGuidId>7f95c4a9-63b9-402e-a899-a91d10723386</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.css-1co2pmb-unf-btn.eg8apji0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5e55bee4-1cf9-44ba-befd-c1864e9095e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-unify</name>
      <type>Main</type>
      <value>Button</value>
      <webElementGuid>d7d41eb7-0ac3-470f-8fc2-6a8a1a39a6ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7af72322-12e0-4f87-85e9-1bc6534dd553</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-1co2pmb-unf-btn eg8apji0</value>
      <webElementGuid>71435c09-d75e-4923-b94f-eac4e153265d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cari Rute Lainnya</value>
      <webElementGuid>1a490b28-ef3f-4dec-9216-e9f35fd92e1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;css-14u6y52&quot;]/div[1]/div[@class=&quot;css-5f1scs&quot;]/div[@class=&quot;css-nsmt50&quot;]/div[@class=&quot;css-lbroap&quot;]/div[@class=&quot;css-xpculq&quot;]/div[@class=&quot;css-1chsdlq&quot;]/div[3]/div[@class=&quot;css-1v3kmok&quot;]/div[@class=&quot;css-xgj2d6&quot;]/button[@class=&quot;css-1co2pmb-unf-btn eg8apji0&quot;]</value>
      <webElementGuid>4b9340f0-ae0e-49a0-ba83-ee25ea86970d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[4]</value>
      <webElementGuid>2f72c1eb-038b-446a-ab4a-27d811ffd76e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/div[2]/div/div/div[3]/div[2]/div[3]/div/div/button</value>
      <webElementGuid>fbe8b09d-64ef-4f9c-b323-76a9e0558823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yaah... Rute ini nggak ada'])[1]/following::button[1]</value>
      <webElementGuid>d6a18d7d-6301-439d-a81b-56fbed130199</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(0 hasil)'])[1]/following::button[1]</value>
      <webElementGuid>5f776de0-eee1-4b8c-9047-2e31293b9687</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::button[1]</value>
      <webElementGuid>c898f63b-7825-47e3-90f5-b4cfb550e31f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/button</value>
      <webElementGuid>8066f915-f86e-4cdc-b1e2-263189ff37cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Cari Rute Lainnya' or . = 'Cari Rute Lainnya')]</value>
      <webElementGuid>4d9071e8-bed9-4a5b-b6e1-d5d971d08cac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
